const menuIcon = document.querySelector('.hamburger-menu');
const navbar = document.querySelector('.navbar');
const links = document.querySelectorAll('li');
const navContainer = document.querySelector('.nav-list');
const navLinks = [...document.querySelectorAll('.nav-item')]

menuIcon.addEventListener('click', () => {
    navbar.classList.toggle("change");
});

// for (let i = 0; i < navLinks.length; i++) {
//     navLinks[i].addEventListener('click', (event) => {
//         console.log('klik');
//         event.target.classList.add('active');

//     })
// }

function selectNav() {
    navLinks.forEach(link => link.style.backgroundColor = '');
    this.style.backgroundColor = 'red';
    console.log('klik')
}

navLinks.forEach(link => link.addEventListener('click', selectNav))


// let i = 0,
//     text;

// text = "Hello! I'm Kamil Nowak. Frontend Developer. ";

// const typing = () => {
//     if (i < text.length) {
//         document.getElementById("text").innerHTML += text.charAt(i);
//         i++;
//         setTimeout(typing, 50);
//     }
// }

// typing();

const textWrapper = document.querySelector('.ml9 .letters');
textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

anime.timeline().add({
    targets: '.ml9 .letter',
    scale: [0, 1],
    duration: 1500,
    elasticity: 600,
    delay: (el, i) => 45 * (i + 1)
})
// .add({
//     targets: '.ml9',
//     opacity: 0,
//     // duration: 10000,
//     // easing: "easeOutExpo",
//     delay: 1000
// });